# Example of usage
#
#  ruby analyze.rb data/2021-02-18.usage_ping.gitlab.com.json
require 'json'
require 'time'

usage_ping_file  = ARGV[0]
payload = JSON.parse(File.read(usage_ping_file))

# compute generation time
start_time = Time.parse(payload['recorded_at'])
end_time = Time.parse(payload['recording_ce_finished_at'])

# seconds divided by 3600 to find number of hours
generation_time = (end_time - start_time) / 3600.0

puts "Finished generated at: #{end_time}"

puts "**Generation time(hours):** #{generation_time.round(2)}"

def flatten_hash(param, prefix=nil)
  param.each_pair.reduce({}) do |a, (k, v)|
    if v.is_a?(Hash)
      a.merge(flatten_hash(v, "#{prefix}#{k}."))
    else
      a.merge("#{prefix}#{k}".to_sym => v)
    end
  end
end

puts "Number of metrics: **#{flatten_hash(payload).size}**"

# Print failed values -1
failures_number = 0

puts "### Failures:"

failed_list = []
flatten_hash(payload).each do |key, value|
  if value.is_a?(Numeric) && value == -1
    failures_number += 1
    failed_list <<  key
  end
end

puts "#{failures_number} failed metrics"

# puts "```ruby"

puts failed_list.map(&:to_s)

# puts "```"







