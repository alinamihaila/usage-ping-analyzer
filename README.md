## Usage Ping payload analyzer

### Analyze failures and deprecations

Prints:

- Failed values: -1
- Generaion time in hours

```shell
ruby analyze.rb data/2021-02-18.usage_ping.gitlab.com.json

{:"usage_activity_by_stage_monthly.manage.project_imports.git"=>-1000}
{:"usage_activity_by_stage_monthly.manage.project_imports.gitlab_project"=>-1}
{:"usage_activity_by_stage_monthly.verify.ci_builds"=>-1}
10.19797611111111

```

### Find metrics value

Search a specifc list of keys and prints its value

```shell
ruby find_metric.rb data/2020-11-26_usage_ping_gitlab.com.json g_project_management_issue_comment_removed

{:"redis_hll_counters.issues_edit.g_project_management_issue_comment_removed_weekly"=>2726}
{:"redis_hll_counters.issues_edit.g_project_management_issue_comment_removed_monthly"=>8463}
=>
```
