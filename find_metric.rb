# Example of usage
#
# ruby find_metric.rb data/2020-11-26_usage_ping_gitlab.com.json g_project_management_issue_comment_removed
require 'json'

file_path = ARGV.shift
metrics = ARGV

usage_ping = JSON.parse(File.read(file_path))

def flatten_hash(param, prefix=nil)
  param.each_pair.reduce({}) do |a, (k, v)|
    if v.is_a?(Hash)
      a.merge(flatten_hash(v, "#{prefix}#{k}."))
    else
      a.merge("#{prefix}#{k}".to_sym => v)
    end
  end
end

flatten_hash(usage_ping).each do |key, value|
  metrics.each do |metric|

    puts  key => value if key.to_s.include?(metric)
  end
end
